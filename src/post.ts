import {get} from "./low-level";
import {TCUID, IPost} from "./types";
import {processPost} from "./processors";


export default class Post {
    protected baseURL: URL;
    public proxy: URL|undefined;

    constructor(apiURL: URL) {
        this.baseURL = apiURL;
    }

    getAnswer(id: TCUID) {
        return this.getComment(id);
    }

    getComment(id: TCUID) {
        // todo
    }

    async getPost(id: TCUID): Promise<IPost> {
        const address = this.baseURL.origin + (this.baseURL.pathname || '') + '/post/' + id.toString();
        const data = await get(address, this.proxy);
        const rawPost = JSON.parse(data).post;
        return processPost(rawPost);
    }
}
