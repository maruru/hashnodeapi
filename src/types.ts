export type TAPIURL = URL|undefined
export type TBadgeID = TUID
export type TCommunityID = TUID
export type TCUID = TUID
export type TPollOptionID = TUID
export type TPostID = TUID
export type TRole = 'superuser' | undefined
export type TTagID = TUID
export type TUID = string
export type TUserID = TUID
export type TUserName = string


export enum EPostType {
    NEWS = 'link',
    QUESTION = 'question',
    STORY = 'story',
}


export interface IAppreciationCountObject {
    _id: TUID
    badge: IBadgeObject
    count: number
}

/**
 * `period` is an array of years, in which the user has the award, I think
 */
export interface IAwardBadgeObject {
    badge: IBadgeObject
    period: Array<number>
}

export interface IBadgeObject {
    __v: 0 // todo: what is this? It's 0 on all badges for me
    _id: TBadgeID
    displayName: string
    image: URL
    name: string
    type: string
}

export interface IComment {
    // todo
}

export interface ICommunity {
    _id: TCommunityID
    id: TCommunityID // inconsistency! Seems to just hold the _id, too
    isActive: boolean
    isApproved: boolean
    logo: URL
    name: string
    slug: string
}

export interface IPollOption {
    _id: TPollOptionID
    index: number
    option: string
    votes: number
}

export interface IPost {
    __v: number // wat?
    _id: TPostID
    answeredByTarget: boolean
    author: IProfile
    bookmarkedIn: any[]// todo: wat???
    brief: string
    content: string
    contentMarkdown: string
    contributors: ({user:IProfile})[] // inconsistency! Looks like they forgot to unwrap an internal call or something
    coverImage: TAPIURL
    cuid: TCUID
    dateAdded: Date
    dateUpdated: Date
    downvotes: number
    duplicatePosts: TPostID[]
    followers: TUserID[]
    followersCount: number
    hasPolls: boolean
    hasReward: boolean
    id: TPostID // inconsistency! Seems to just hold the _id, too
    indexVotedByCurrentUser: number
    isActive: boolean
    isAnonymous: boolean
    isDelisted: boolean
    isEngaging: boolean
    isFeatured: boolean
    isOriginal: boolean
    isPublication: boolean
    isRepublished: boolean
    labelsByNodes: { labels: any[] }// todo: wat???
    numCollapsed: number
    originalUrl: TAPIURL
    pollOptions: IPollOption[]
    popularity: number // what does it mean?
    publication: IPublication
    questionReplies: TPostID[]
    reactions: string[]
    reactionsByCurrentUser: string[]
    reactionToCountMap: Map<string, number>
    replyCount: number
    responseCount: number
    responses: IComment[]
    reward: IReward
    slug: string
    tags: ITag[]
    title: string
    totalReactions: number
    totalPollVotes: number
    type: EPostType
    untaggedFrom: TCommunityID[]
    upvotes: number
    url: string
    views: number
}

export interface IPublication {
    name: string
    slug: string
}

export interface IRawCommunity {
    _id: TCommunityID
    id: TCommunityID // inconsistency! Seems to just hold the _id, too
    isActive: boolean
    isApproved: boolean
    logo: string
    name: string
    slug: string
}

export interface IRawPost {
    __v: number // wat?
    _id: TPostID
    answeredByTarget: boolean
    author: IRawUserProfile
    bookmarkedIn: any[]// todo: wat???
    brief: string
    content: string
    contentMarkdown: string
    contributors: ({user:IProfile})[] // inconsistency! Looks like they forgot to unwrap an internal call or something
    coverImage: string
    cuid: TCUID
    dateAdded: string
    dateUpdated: string
    downvotes: number
    duplicatePosts: TPostID[]
    followers: TUserID[]
    followersCount: number
    hasPolls: boolean
    hasReward: boolean
    id: TPostID // inconsistency! Seems to just hold the _id, too
    indexVotedByCurrentUser: number
    isActive: boolean
    isAnonymous: boolean
    isDelisted: boolean
    isEngaging: boolean
    isFeatured: boolean
    isOriginal: boolean
    isPublication: boolean
    isRepublished: boolean
    labelsByNodes: { labels: any[] }// todo: wat???
    numCollapsed: number
    originalUrl: string
    pollOptions: IPollOption[]
    popularity: number
    publication: IPublication
    questionReplies: TPostID[]
    reactions: string[]
    reactionsByCurrentUser: string[]
    reactionToCountMap: any|undefined
    replyCount: number
    responseCount: number
    responses: IComment[]
    reward: IReward
    slug: string
    tags: ITag[]
    title: string
    totalReactions: number
    totalPollVotes: number
    type: EPostType
    untaggedFrom: TCommunityID[]
    upvotes: number
    url: string
    views: number
}

export interface IRawUserProfile {
    _id: TUserID
    appliedForOriginals: boolean
    appreciations: IAppreciationCountObject[]
    availableFor: string
    badgesAwarded: IAwardBadgeObject[]
    beingFollowed: boolean
    bio: string
    bioMarkdown: string
    coverImage: string
    dateJoined: string
    directQuestionsCount: number
    email: string|undefined
    featuredPosts: any[] // todo: dunno what goes here
    focus: IRawCommunity[]
    followsBack: boolean
    id: TUserID // inconsistency! Seems to just hold the _id, too
    isBanned: boolean
    isDeactivated: boolean
    isEvangelist: boolean
    location: string
    managerOf: IRawCommunity[]
    name: string
    numFollowers: number
    numFollowing: number
    photo: string
    role: TRole
    showEmailOnProfile: boolean
    socialMedia: {
        facebook: string
        github: string
        google: string
        linkedin: string
        stackoverflow: string
        twitter: string
        website: string
    }
    storiesCreated: Array<TUID>
    tagline: string
    totalAppreciationBadges: number
    totalUpvotesReceived: number
    username: TUserName
}

export interface IReward {
    type: string
}

export interface ITag {
    _id: TTagID
    name: string
    slug: string
    id: TTagID // inconsistency! Seems to just hold the _id, too
    isApproved: boolean
    logo: URL|undefined
    isActive: boolean
    numPosts: number
    mergedWith: ITag|null
}

/**
 * @field {boolean} beingFollowed - is true if the target user is followed by the current user
 * @field {string} bio - is the rendered markdown of field bioMarkdown
 * @field {boolean} followsBack - is true if the target user follows the current user
 */
export interface IProfile {
    _id: TUserID
    appliedForOriginals: boolean
    appreciations: Array<IAppreciationCountObject>
    availableFor: string
    badgesAwarded: Array<IAwardBadgeObject>
    beingFollowed: boolean
    bio: string
    bioMarkdown: string
    coverImage: TAPIURL
    dateJoined: Date
    directQuestionsCount: number
    email: string|undefined
    featuredPosts: Array<void> // todo: dunno what goes here
    focus: Array<ICommunity>
    followsBack: boolean
    id: TUserID // inconsistency! Seems to just hold the _id, too
    isBanned: boolean
    isDeactivated: boolean
    isEvangelist: boolean
    location: string
    managerOf: Array<ICommunity>
    name: string
    numFollowers: number
    numFollowing: number
    numSeries: number
    photo: TAPIURL
    role: TRole
    showEmailOnProfile: boolean
    socialMedia: {
        facebook: URL|undefined
        github: URL|undefined
        google: URL|undefined
        linkedin: URL|undefined
        stackoverflow: URL|undefined
        twitter: URL|undefined
        website: URL|undefined
    }
    storiesCreated: Array<TUID>
    tagline: string
    totalAppreciationBadges: number
    totalUpvotesReceived: number
    username: TUserName
}
