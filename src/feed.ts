import {get} from "./low-level";
import {IPost, IRawPost} from "./types";
import {processPost} from "./processors";

/*
 * Note: The implementation of fetching feeds is flawed. There is no way to tell the API which post was fetched last,
 * and as a result, if new threads are opened while fetching, posts may appear twice or more often
 * in consecutive API calls
 */

export default class Feed {
    protected baseURL: URL;
    public proxy: URL|undefined;

    constructor(apiURL: URL) {
        this.baseURL = apiURL;
    }

    private async *_getFeed(address: string): AsyncIterableIterator<IPost> {
        let fetchCounter = 0;
        let rawPosts: IRawPost[];

        while(true) {
            rawPosts = JSON.parse(
                await get({
                    url: address,
                    query: {
                        page: fetchCounter++,
                    }
                }, this.proxy)
            ).posts;

            if (rawPosts.length <= 0) break;

            for (let post of rawPosts) {
                yield processPost(post);
            }
        }
    }

    async *getFeaturedFeed(): AsyncIterableIterator<IPost> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/featured`;
        const feed = this._getFeed(address);

        for await (let post of feed) {
            yield post;
        }
    }

    async *getHotFeed(): AsyncIterableIterator<IPost> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/hot`;
        const feed = this._getFeed(address);

        for await (let post of feed) {
            yield post;
        }
    }

    async *getRecentFeed(): AsyncIterableIterator<IPost> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/recent`;
        const feed = this._getFeed(address);

        for await (let post of feed) {
            yield post;
        }
    }

    async *getStoriesFeed(): AsyncIterableIterator<IPost> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/stories`;
        const feed = this._getFeed(address);

        for await (let post of feed) {
            yield post;
        }
    }

    async *getTrendingStoriesFeed(): AsyncIterableIterator<IPost> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/stories/trending`;
        const feed = this._getFeed(address);

        for await (let post of feed) {
            yield post;
        }
    }
}
