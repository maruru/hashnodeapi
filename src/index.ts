import Post from "./post";
import User from './user';
import Feed from "./feed";


export class Hashnode {
    public baseURL: URL;
    public readonly feed: Feed;
    public readonly post: Post;
    public readonly user: User;

    constructor(domain: string, path: string = '/') {
        if (path[0] !== '/') {
            path = '/' + path;
        }

        this.baseURL = new URL('', domain + path);
        this.feed = new Feed(this.baseURL);
        this.post = new Post(this.baseURL);
        this.user = new User(this.baseURL);
    }

    static default(): Hashnode {
        return new Hashnode('https://hashnode.com', '/ajax');
    }

    setProxy(proxyAddr: URL|undefined) {
        this.feed.proxy = proxyAddr;
        this.post.proxy = proxyAddr;
        this.user.proxy = proxyAddr;
    }
}
