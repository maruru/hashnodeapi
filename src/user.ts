import {get} from "./low-level";
import {IRawUserProfile, TUserName, IProfile, IRawPost, IPost} from "./types";
import {processPost, processUserProfile} from "./processors";


export interface IGetUserOptions {
    limit?: number | undefined
    sinceDate?: '7d' | '30d' | '' | undefined// duration is not calculated, but seems to be one of these fixed values, or by default "all time"
    sortBy: 'evangelists' | 'helpful' | 'newUsers'
}

export default class User {
    protected basePath: string;
    protected baseURL: URL;
    public proxy: URL|undefined;

    constructor(apiURL: URL) {
        this.baseURL = apiURL;
        this.basePath = this.baseURL.origin + (this.baseURL.pathname || '');
    }

    async getProfile(username: TUserName): Promise<IProfile> {
        const address = this.basePath + '/profile/' + username.toString();
        const data = await get(address, this.proxy);
        const rawProfile: IRawUserProfile = JSON.parse(data).profile;
        return processUserProfile(rawProfile);
    }

    async getProfiles(options: IGetUserOptions): Promise<IProfile[]> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/users`;
        const limit = (options.limit || 0) < 1 ? '' : options.limit;
        const rawUsers = JSON.parse(
            await get({
                url: address,
                query: {
                    duration: options.sinceDate || '',// only works for helpful
                    limit: '',// seems defect, only works on helpful and seems to skip positions, delivering fewer accounts than it should
                    type: options.sortBy,
                }
            }, this.proxy)
        ).users;

        // Enforce limit, because it seems to be defect, even on helpful
        if (!isNaN(parseInt((limit || '').toString()))) {// limit is number
            rawUsers.splice(limit as number);
        }

        return rawUsers.map(processUserProfile);
    }

    async getStories(username: TUserName): Promise<IPost[]> {
        const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/@${username.toString()}/fetch-stories`;
        let data = [];
        let fetchCounter = 0;
        let rawPosts: IRawPost[] = [];

        // Get all stories
        do {
            data = JSON.parse(
                await get({
                    url: address,
                    query: {
                        sortBy: 'dateAdded',
                        skip: fetchCounter++,
                    }
                }, this.proxy)
            ).posts;
            rawPosts.push.apply(rawPosts, data);
        } while (data.length > 0);

        return rawPosts.map(processPost);
    }
}
