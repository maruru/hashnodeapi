import {ICommunity, IPost, IProfile, IRawCommunity, IRawPost, IRawUserProfile} from "./types";
import {maybeURL} from "./util";


// todo: need a nice way to have the tsc find missing conversions at compile time

const processCommunity = function (rawCommunity: IRawCommunity): ICommunity {
    let community = <ICommunity>{};
    Object.assign(community, rawCommunity, { logo: new URL(rawCommunity.logo) });
    return community;
};

const processPost = function(rawPost: IRawPost): IPost {
    let post: IPost = <IPost>{};

    Object.assign(post, rawPost, {
        author: processUserProfile(rawPost.author),
        coverImage: maybeURL(rawPost.coverImage),
        dateAdded: new Date(rawPost.dateAdded),
        dateUpdated: new Date(rawPost.dateUpdated),
        originalUrl: maybeURL(rawPost.originalUrl),
        reactionToCountMap: (!rawPost.reactionToCountMap)
            ? new Map()
            : Object
                .entries(rawPost.reactionToCountMap)
                .reduce(
                    // @ts-ignore
                    (acc: Map<string, number>, entry: (string|number)[]) => { acc.set.apply(acc, entry); return acc; },
                    new Map()
                ),
    });

    return post;
};

const processUserProfile = function(rawProfile: IRawUserProfile): IProfile {
    let profile = <IProfile>{};

    Object.assign(profile, rawProfile, {
        coverImage: maybeURL(rawProfile.coverImage),
        dateJoined: new Date(rawProfile.dateJoined),
        focus: rawProfile.focus.map(processCommunity),
        managerOf: rawProfile.managerOf.map(processCommunity),
        photo: maybeURL(rawProfile.photo),
        socialMedia: Object
            .entries(rawProfile.socialMedia)
            // @ts-ignore
            .reduce((acc: any, entry: any[]) => {
                acc[entry[0]] = (entry[1] && entry[1] !== '') ? new URL(entry[1]) : undefined;
                return acc;
                // @ts-ignore
            }, {})
    });

    return profile;
};


export {processCommunity, processPost, processUserProfile};
