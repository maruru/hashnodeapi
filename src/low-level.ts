import * as SA from 'superagent';

require('superagent-proxy')(SA);


const config = require('../package.json');

interface IGetOptions {
    url: string
    query: Object|undefined
}

const get = function(connection: string|IGetOptions, proxy: URL|undefined = undefined): Promise<string> {
    const path = typeof connection === 'string' ? connection : connection.url;

    return new Promise<string>((res, rej) => {
        const request = SA.get(path);

        request
            .set('Accept', 'application/json')
            .set('DNT', '1')
            .set('User-Agent', `HashnodeAPI/${config.version} (maruru; JS)`)
        ;

        if (typeof connection !== 'string') {
            connection.query && request.query(connection.query);
        }

        if (proxy) {
            // @ts-ignore
            request.proxy(proxy.href);
        }

        request.end((err: Error|null, response: SA.Response) => {
            if (err) {
                rej(err);
            }
            else if (response.status >= 400) {
                rej(new Error(response.status.toString()));
            }
            else {
                res(response.text);
            }
        });

    });
};


export {get};
