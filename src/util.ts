import {TAPIURL} from "./types";


const deepCopy = function<T extends Object> (obj: T): T {
    return JSON.parse(JSON.stringify(obj));
};

const maybeURL = function(value: string|undefined): TAPIURL {
    return ((value && value !== '')
        ? new URL(value)
        : undefined
    ) as TAPIURL;
};

export {deepCopy, maybeURL};
