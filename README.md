# HashnodeAPI

Unofficial Hashnode API


## Installation

```
$ npm i hashnode
```


## Platform support

At the moment, only NodeJS is supported, however browser support is technically possible.
Webpack and Browserify are supported.
If you build for the browser, please provide superagent on the global variable `window.SA`.


## Usage

In order to use this package, you will need to call a method on one of the below API interfaces.
They return an interface, which is defined further down, below Types.


### Example

Please take a look at the examples in `/example`.

```javascript
/**
* Get the newst post from the recent feed
*/

// Create new API object and set the domain and namespace path
const hn = new (require('hashnode').Hashnode)('https://hashnode.com', '/ajax');

// You may add a proxy
hn.setProxy(new URL('http://localhost:3128'));

// Then query the API by using high-level convenience methods
(async () => {
    // Get a feed iterator
    const feed = hn.feed.getRecentFeed();
    // Get the first post
    const mostRecentPost = await feed.next();
    // Output the post title
    console.log(mostRecentPost.value.title);
})().catch(console.error);

```


### API

The usage is fairly simple and here is the API:


#### Entry: `Hashnode`

```typescript
class Hashnode {
    baseURL: URL;
    readonly feed: Feed;
    readonly post: Post;
    readonly user: User;
    constructor(domain: string, path?: string);
    setProxy(proxyAddr: URL | undefined): void;
}
```


#### Feed

```typescript
class Feed {
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    private _getFeed;
    getFeaturedFeed(): AsyncIterableIterator<IPost>;
    getHotFeed(): AsyncIterableIterator<IPost>;
    getRecentFeed(): AsyncIterableIterator<IPost>;
    getStoriesFeed(): AsyncIterableIterator<IPost>;
    getTrendingStoriesFeed(): AsyncIterableIterator<IPost>;
}
```


#### Post

```typescript
class Post {
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    getAnswer(id: TCUID): void;
    getComment(id: TCUID): void;
    getPost(id: TCUID): Promise<IPost>;
    static processPost(rawPost: IRawPost): IPost;
}
```


#### User

```typescript
interface IGetUserOptions {
    limit?: number | undefined;
    sinceDate?: '7d' | '30d' | '' | undefined;
    sortBy: 'evangelists' | 'helpful' | 'newUsers';
}

class User {
    protected basePath: string;
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    getProfile(username: TUserName): Promise<IProfile>;
    getProfiles(options: IGetUserOptions): Promise<IProfile[]>;
    getStories(username: TUserName): Promise<IPost[]>;
    static processUserProfile(rawProfile: IRawUserProfile): IProfile;
}
```


### Types

In addition to the above API interfaces, this package also provides you with hand-crafted type interfaces.
All raw API responses are transformed to match these type interfaces for your convenience.

```typescript
export declare type TAPIURL = URL | undefined;
export declare type TBadgeID = TUID;
export declare type TCommunityID = TUID;
export declare type TCUID = TUID;
export declare type TPollOptionID = TUID;
export declare type TPostID = TUID;
export declare type TRole = 'superuser';
export declare type TTagID = TUID;
export declare type TUID = string;
export declare type TUserID = TUID;
export declare type TUserName = string;
export declare enum EPostType {
    NEWS = "link",
    QUESTION = "question",
    STORY = "story"
}
export interface IAppreciationCountObject {
    _id: TUID;
    badge: IBadgeObject;
    count: number;
}
export interface IAwardBadgeObject {
    badge: IBadgeObject;
    period: Array<number>;
}
export interface IBadgeObject {
    __v: 0;
    _id: TBadgeID;
    displayName: string;
    image: URL;
    name: string;
    type: string;
}
export interface IComment {
}
export interface ICommunity {
    _id: TCommunityID;
    id: TCommunityID;
    isActive: boolean;
    isApproved: boolean;
    logo: URL;
    name: string;
    slug: string;
}
export interface IPollOption {
    _id: TPollOptionID;
    index: number;
    option: string;
    votes: number;
}
export interface IPost {
    __v: number;
    _id: TPostID;
    answeredByTarget: boolean;
    author: IProfile;
    bookmarkedIn: any[];
    brief: string;
    content: string;
    contentMarkdown: string;
    contributors: ({
        user: IProfile;
    })[];
    coverImage: TAPIURL;
    cuid: TCUID;
    dateAdded: Date;
    dateUpdated: Date;
    downvotes: number;
    duplicatePosts: TPostID[];
    followers: TUserID[];
    followersCount: number;
    hasPolls: boolean;
    hasReward: boolean;
    id: TPostID;
    indexVotedByCurrentUser: number;
    isActive: boolean;
    isAnonymous: boolean;
    isDelisted: boolean;
    isEngaging: boolean;
    isFeatured: boolean;
    isOriginal: boolean;
    isPublication: boolean;
    labelsByNodes: {
        labels: any[];
    };
    numCollapsed: number;
    originalUrl: TAPIURL;
    pollOptions: IPollOption[];
    publication: IPublication;
    questionReplies: TPostID[];
    reactions: string[];
    reactionsByCurrentUser: string[];
    reactionToCountMap: Map<string, number>;
    responseCount: number;
    responses: IComment[];
    reward: IReward;
    slug: string;
    tags: ITag[];
    title: string;
    totalReactions: number;
    totalPollVotes: number;
    type: EPostType;
    untaggedFrom: TCommunityID[];
    upvotes: number;
    url: string;
    views: number;
}
export interface IPublication {
    name: string;
    slug: string;
}
export interface IRawPost {
    __v: number;
    _id: TPostID;
    answeredByTarget: boolean;
    author: IRawUserProfile;
    bookmarkedIn: any[];
    brief: string;
    content: string;
    contentMarkdown: string;
    contributors: ({
        user: IProfile;
    })[];
    coverImage: string;
    cuid: TCUID;
    dateAdded: string;
    dateUpdated: string;
    downvotes: number;
    duplicatePosts: TPostID[];
    followers: TUserID[];
    followersCount: number;
    hasPolls: boolean;
    hasReward: boolean;
    id: TPostID;
    indexVotedByCurrentUser: number;
    isActive: boolean;
    isAnonymous: boolean;
    isDelisted: boolean;
    isEngaging: boolean;
    isFeatured: boolean;
    isOriginal: boolean;
    isPublication: boolean;
    labelsByNodes: {
        labels: any[];
    };
    numCollapsed: number;
    originalUrl: string;
    pollOptions: IPollOption[];
    publication: IPublication;
    questionReplies: TPostID[];
    reactions: string[];
    reactionsByCurrentUser: string[];
    reactionToCountMap: any | undefined;
    responseCount: number;
    responses: IComment[];
    reward: IReward;
    slug: string;
    tags: ITag[];
    title: string;
    totalReactions: number;
    totalPollVotes: number;
    type: EPostType;
    untaggedFrom: TCommunityID[];
    upvotes: number;
    url: string;
    views: number;
}
export interface IRawUserProfile {
    _id: TUserID;
    appliedForOriginals: boolean;
    appreciations: IAppreciationCountObject[];
    availableFor: string;
    badgesAwarded: IAwardBadgeObject[];
    beingFollowed: boolean;
    bio: string;
    bioMarkdown: string;
    coverImage: string;
    dateJoined: string;
    directQuestionsCount: number;
    email: string | undefined;
    featuredPosts: any[];
    focus: ICommunity[];
    followsBack: boolean;
    id: TUserID;
    isBanned: boolean;
    isDeactivated: boolean;
    isEvangelist: boolean;
    location: string;
    managerOf: ICommunity[];
    name: string;
    numFollowers: number;
    numFollowing: number;
    photo: string;
    role: TRole | undefined;
    showEmailOnProfile: boolean;
    socialMedia: {
        facebook: string;
        github: string;
        google: string;
        linkedin: string;
        stackoverflow: string;
        twitter: string;
        website: string;
    };
    storiesCreated: Array<TUID>;
    tagline: string;
    totalAppreciationBadges: number;
    totalUpvotesReceived: number;
    username: TUserName;
}
export interface IReward {
    type: string;
}
export interface ITag {
    _id: TTagID;
    name: string;
    slug: string;
    id: TTagID;
    isApproved: boolean;
    logo: URL | undefined;
    isActive: boolean;
    numPosts: number;
    mergedWith: ITag | null;
}
export interface IProfile {
    _id: TUserID;
    appliedForOriginals: boolean;
    appreciations: Array<IAppreciationCountObject>;
    availableFor: string;
    badgesAwarded: Array<IAwardBadgeObject>;
    beingFollowed: boolean;
    bio: string;
    bioMarkdown: string;
    coverImage: TAPIURL;
    dateJoined: Date;
    directQuestionsCount: number;
    email: string | undefined;
    featuredPosts: Array<void>;
    focus: Array<ICommunity>;
    followsBack: boolean;
    id: TUserID;
    isBanned: boolean;
    isDeactivated: boolean;
    isEvangelist: boolean;
    location: string;
    managerOf: Array<ICommunity>;
    name: string;
    numFollowers: number;
    numFollowing: number;
    photo: TAPIURL;
    role: TRole | undefined;
    showEmailOnProfile: boolean;
    socialMedia: {
        facebook: URL | undefined;
        github: undefined;
        google: URL | undefined;
        linkedin: URL | undefined;
        stackoverflow: URL | undefined;
        twitter: URL | undefined;
        website: URL | undefined;
    };
    storiesCreated: Array<TUID>;
    tagline: string;
    totalAppreciationBadges: number;
    totalUpvotesReceived: number;
    username: TUserName;
}
```
