import { TCUID, IPost as IPost, IRawPost } from "./types";
export default class Post {
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    getAnswer(id: TCUID): void;
    getComment(id: TCUID): void;
    getPost(id: TCUID): Promise<IPost>;
    static processPost(rawPost: IRawPost): IPost;
}
