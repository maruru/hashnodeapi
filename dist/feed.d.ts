/// <reference types="node" />
import { IPost as IPost } from "./types";
export default class Feed {
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    private _getFeed;
    getFeaturedFeed(): AsyncIterableIterator<IPost>;
    getHotFeed(): AsyncIterableIterator<IPost>;
    getRecentFeed(): AsyncIterableIterator<IPost>;
    getStoriesFeed(): AsyncIterableIterator<IPost>;
    getTrendingStoriesFeed(): AsyncIterableIterator<IPost>;
}
