interface IGetOptions {
    url: string;
    query: Object | undefined;
}
declare const get: (connection: string | IGetOptions, proxy?: URL | undefined) => Promise<string>;
export { get };
