import Post from "./post";
import User from './user';
import Feed from "./feed";
export declare class Hashnode {
    baseURL: URL;
    readonly feed: Feed;
    readonly post: Post;
    readonly user: User;
    constructor(domain: string, path?: string);
    setProxy(proxyAddr: URL | undefined): void;
}
