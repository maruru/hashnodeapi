import { TAPIURL } from "./types";
declare const maybeURL: (value: string | undefined) => TAPIURL;
export { maybeURL };
