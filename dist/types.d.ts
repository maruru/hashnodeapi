/// <reference types="node" />
import { URL } from "url";
export declare type TAPIURL = URL | undefined;
export declare type TBadgeID = TUID;
export declare type TCommunityID = TUID;
export declare type TCUID = TUID;
export declare type TPollOptionID = TUID;
export declare type TPostID = TUID;
export declare type TRole = 'superuser';
export declare type TTagID = TUID;
export declare type TUID = string;
export declare type TUserID = TUID;
export declare type TUserName = string;
export declare enum EPostType {
    NEWS = "link",
    QUESTION = "question",
    STORY = "story"
}
export interface IAppreciationCountObject {
    _id: TUID;
    badge: IBadgeObject;
    count: number;
}
export interface IAwardBadgeObject {
    badge: IBadgeObject;
    period: Array<number>;
}
export interface IBadgeObject {
    __v: 0;
    _id: TBadgeID;
    displayName: string;
    image: URL;
    name: string;
    type: string;
}
export interface IComment {
}
export interface ICommunity {
    _id: TCommunityID;
    id: TCommunityID;
    isActive: boolean;
    isApproved: boolean;
    logo: URL;
    name: string;
    slug: string;
}
export interface IPollOption {
    _id: TPollOptionID;
    index: number;
    option: string;
    votes: number;
}
export interface IPost {
    __v: number;
    _id: TPostID;
    answeredByTarget: boolean;
    author: IProfile;
    bookmarkedIn: any[];
    brief: string;
    content: string;
    contentMarkdown: string;
    contributors: ({
        user: IProfile;
    })[];
    coverImage: TAPIURL;
    cuid: TCUID;
    dateAdded: Date;
    dateUpdated: Date;
    downvotes: number;
    duplicatePosts: TPostID[];
    followers: TUserID[];
    followersCount: number;
    hasPolls: boolean;
    hasReward: boolean;
    id: TPostID;
    indexVotedByCurrentUser: number;
    isActive: boolean;
    isAnonymous: boolean;
    isDelisted: boolean;
    isEngaging: boolean;
    isFeatured: boolean;
    isOriginal: boolean;
    isPublication: boolean;
    labelsByNodes: {
        labels: any[];
    };
    numCollapsed: number;
    originalUrl: TAPIURL;
    pollOptions: IPollOption[];
    publication: IPublication;
    questionReplies: TPostID[];
    reactions: string[];
    reactionsByCurrentUser: string[];
    reactionToCountMap: Map<string, number>;
    responseCount: number;
    responses: IComment[];
    reward: IReward;
    slug: string;
    tags: ITag[];
    title: string;
    totalReactions: number;
    totalPollVotes: number;
    type: EPostType;
    untaggedFrom: TCommunityID[];
    upvotes: number;
    url: string;
    views: number;
}
export interface IPublication {
    name: string;
    slug: string;
}
export interface IRawPost {
    __v: number;
    _id: TPostID;
    answeredByTarget: boolean;
    author: IRawUserProfile;
    bookmarkedIn: any[];
    brief: string;
    content: string;
    contentMarkdown: string;
    contributors: ({
        user: IProfile;
    })[];
    coverImage: string;
    cuid: TCUID;
    dateAdded: string;
    dateUpdated: string;
    downvotes: number;
    duplicatePosts: TPostID[];
    followers: TUserID[];
    followersCount: number;
    hasPolls: boolean;
    hasReward: boolean;
    id: TPostID;
    indexVotedByCurrentUser: number;
    isActive: boolean;
    isAnonymous: boolean;
    isDelisted: boolean;
    isEngaging: boolean;
    isFeatured: boolean;
    isOriginal: boolean;
    isPublication: boolean;
    labelsByNodes: {
        labels: any[];
    };
    numCollapsed: number;
    originalUrl: string;
    pollOptions: IPollOption[];
    publication: IPublication;
    questionReplies: TPostID[];
    reactions: string[];
    reactionsByCurrentUser: string[];
    reactionToCountMap: any | undefined;
    responseCount: number;
    responses: IComment[];
    reward: IReward;
    slug: string;
    tags: ITag[];
    title: string;
    totalReactions: number;
    totalPollVotes: number;
    type: EPostType;
    untaggedFrom: TCommunityID[];
    upvotes: number;
    url: string;
    views: number;
}
export interface IRawUserProfile {
    _id: TUserID;
    appliedForOriginals: boolean;
    appreciations: IAppreciationCountObject[];
    availableFor: string;
    badgesAwarded: IAwardBadgeObject[];
    beingFollowed: boolean;
    bio: string;
    bioMarkdown: string;
    coverImage: string;
    dateJoined: string;
    directQuestionsCount: number;
    email: string | undefined;
    featuredPosts: any[];
    focus: ICommunity[];
    followsBack: boolean;
    id: TUserID;
    isBanned: boolean;
    isDeactivated: boolean;
    isEvangelist: boolean;
    location: string;
    managerOf: ICommunity[];
    name: string;
    numFollowers: number;
    numFollowing: number;
    photo: string;
    role: TRole | undefined;
    showEmailOnProfile: boolean;
    socialMedia: {
        facebook: string;
        github: string;
        google: string;
        linkedin: string;
        stackoverflow: string;
        twitter: string;
        website: string;
    };
    storiesCreated: Array<TUID>;
    tagline: string;
    totalAppreciationBadges: number;
    totalUpvotesReceived: number;
    username: TUserName;
}
export interface IReward {
    type: string;
}
export interface ITag {
    _id: TTagID;
    name: string;
    slug: string;
    id: TTagID;
    isApproved: boolean;
    logo: URL | undefined;
    isActive: boolean;
    numPosts: number;
    mergedWith: ITag | null;
}
export interface IProfile {
    _id: TUserID;
    appliedForOriginals: boolean;
    appreciations: Array<IAppreciationCountObject>;
    availableFor: string;
    badgesAwarded: Array<IAwardBadgeObject>;
    beingFollowed: boolean;
    bio: string;
    bioMarkdown: string;
    coverImage: TAPIURL;
    dateJoined: Date;
    directQuestionsCount: number;
    email: string | undefined;
    featuredPosts: Array<void>;
    focus: Array<ICommunity>;
    followsBack: boolean;
    id: TUserID;
    isBanned: boolean;
    isDeactivated: boolean;
    isEvangelist: boolean;
    location: string;
    managerOf: Array<ICommunity>;
    name: string;
    numFollowers: number;
    numFollowing: number;
    photo: TAPIURL;
    role: TRole | undefined;
    showEmailOnProfile: boolean;
    socialMedia: {
        facebook: URL | undefined;
        github: undefined;
        google: URL | undefined;
        linkedin: URL | undefined;
        stackoverflow: URL | undefined;
        twitter: URL | undefined;
        website: URL | undefined;
    };
    storiesCreated: Array<TUID>;
    tagline: string;
    totalAppreciationBadges: number;
    totalUpvotesReceived: number;
    username: TUserName;
}
