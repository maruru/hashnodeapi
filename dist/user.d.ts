import { IRawUserProfile, TUserName, IProfile, IPost as IPost } from "./types";
export interface IGetUserOptions {
    limit?: number | undefined;
    sinceDate?: '7d' | '30d' | '' | undefined;
    sortBy: 'evangelists' | 'helpful' | 'newUsers';
}
export default class User {
    protected basePath: string;
    protected baseURL: URL;
    proxy: URL | undefined;
    constructor(apiURL: URL);
    getProfile(username: TUserName): Promise<IProfile>;
    getProfiles(options: IGetUserOptions): Promise<IProfile[]>;
    getStories(username: TUserName): Promise<IPost[]>;
    static processUserProfile(rawProfile: IRawUserProfile): IProfile;
}
