'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var SA = require('superagent');

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

require('superagent-proxy')(SA);
const config = require('../package.json');
const get = function (connection, proxy = undefined) {
    const path = typeof connection === 'string' ? connection : connection.url;
    return new Promise((res, rej) => {
        const request = SA.get(path);
        request
            .set('Accept', 'application/json')
            .set('DNT', '1')
            .set('User-Agent', `HashnodeAPI/${config.version}`);
        if (typeof connection !== 'string') {
            connection.query && request.query(connection.query);
        }
        if (proxy) {
            request.proxy(proxy.href);
        }
        request.end((err, response) => {
            if (err) {
                rej(err);
            }
            else if (response.status >= 400) {
                rej(new Error(response.status.toString()));
            }
            else {
                res(response.text);
            }
        });
    });
};

const maybeURL = function (value) {
    return ((value && value !== '')
        ? new URL(value)
        : undefined);
};

class User {
    constructor(apiURL) {
        this.baseURL = apiURL;
        this.basePath = this.baseURL.origin + (this.baseURL.pathname || '');
    }
    getProfile(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const address = this.basePath + '/profile/' + username.toString();
            const data = yield get(address, this.proxy);
            const rawProfile = JSON.parse(data).profile;
            return User.processUserProfile(rawProfile);
        });
    }
    getProfiles(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/users`;
            const limit = (options.limit || 0) < 1 ? '' : options.limit;
            const rawUsers = JSON.parse(yield get({
                url: address,
                query: {
                    duration: options.sinceDate || '',
                    limit: '',
                    type: options.sortBy,
                }
            }, this.proxy)).users;
            if (!isNaN(parseInt((limit || '').toString()))) {
                rawUsers.splice(limit);
            }
            return rawUsers.map(User.processUserProfile);
        });
    }
    getStories(username) {
        return __awaiter(this, void 0, void 0, function* () {
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/@${username.toString()}/fetch-stories`;
            let data = [];
            let fetchCounter = 0;
            let rawPosts = [];
            do {
                data = JSON.parse(yield get({
                    url: address,
                    query: {
                        sortBy: 'dateAdded',
                        skip: fetchCounter++,
                    }
                }, this.proxy)).posts;
                rawPosts.push.apply(rawPosts, data);
            } while (data.length > 0);
            return rawPosts.map(Post.processPost);
        });
    }
    static processUserProfile(rawProfile) {
        let profile = {};
        Object.assign(profile, rawProfile, {
            coverImage: maybeURL(rawProfile.coverImage),
            dateJoined: new Date(rawProfile.dateJoined),
            photo: maybeURL(rawProfile.photo),
            socialMedia: Object
                .entries(rawProfile.socialMedia)
                .reduce((acc, entry) => {
                acc[entry[0]] = (entry[1] && entry[1] !== '') ? new URL(entry[1]) : undefined;
                return acc;
            }, {})
        });
        return profile;
    }
}

class Post {
    constructor(apiURL) {
        this.baseURL = apiURL;
    }
    getAnswer(id) {
        return this.getComment(id);
    }
    getComment(id) {
    }
    getPost(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const address = this.baseURL.origin + (this.baseURL.pathname || '') + '/post/' + id.toString();
            const data = yield get(address, this.proxy);
            const rawPost = JSON.parse(data).post;
            return Post.processPost(rawPost);
        });
    }
    static processPost(rawPost) {
        let post = {};
        Object.assign(post, rawPost, {
            author: User.processUserProfile(rawPost.author),
            coverImage: maybeURL(rawPost.coverImage),
            dateAdded: new Date(rawPost.dateAdded),
            dateUpdated: new Date(rawPost.dateUpdated),
            originalUrl: maybeURL(rawPost.originalUrl),
            reactionToCountMap: (!rawPost.reactionToCountMap)
                ? new Map()
                : Object
                    .entries(rawPost.reactionToCountMap)
                    .reduce((acc, entry) => { acc.set.apply(acc, entry); return acc; }, new Map()),
        });
        return post;
    }
}

class Feed {
    constructor(apiURL) {
        this.baseURL = apiURL;
    }
    _getFeed(address) {
        return __asyncGenerator(this, arguments, function* _getFeed_1() {
            let fetchCounter = 0;
            let rawPosts;
            while (true) {
                rawPosts = JSON.parse(yield __await(get({
                    url: address,
                    query: {
                        page: fetchCounter++,
                    }
                }, this.proxy))).posts;
                if (rawPosts.length <= 0)
                    break;
                for (let post of rawPosts) {
                    yield yield __await(Post.processPost(post));
                }
            }
        });
    }
    getFeaturedFeed() {
        return __asyncGenerator(this, arguments, function* getFeaturedFeed_1() {
            var e_1, _a;
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/featured`;
            const feed = this._getFeed(address);
            try {
                for (var feed_1 = __asyncValues(feed), feed_1_1; feed_1_1 = yield __await(feed_1.next()), !feed_1_1.done;) {
                    let post = feed_1_1.value;
                    yield yield __await(post);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (feed_1_1 && !feed_1_1.done && (_a = feed_1.return)) yield __await(_a.call(feed_1));
                }
                finally { if (e_1) throw e_1.error; }
            }
        });
    }
    getHotFeed() {
        return __asyncGenerator(this, arguments, function* getHotFeed_1() {
            var e_2, _a;
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/hot`;
            const feed = this._getFeed(address);
            try {
                for (var feed_2 = __asyncValues(feed), feed_2_1; feed_2_1 = yield __await(feed_2.next()), !feed_2_1.done;) {
                    let post = feed_2_1.value;
                    yield yield __await(post);
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (feed_2_1 && !feed_2_1.done && (_a = feed_2.return)) yield __await(_a.call(feed_2));
                }
                finally { if (e_2) throw e_2.error; }
            }
        });
    }
    getRecentFeed() {
        return __asyncGenerator(this, arguments, function* getRecentFeed_1() {
            var e_3, _a;
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/recent`;
            const feed = this._getFeed(address);
            try {
                for (var feed_3 = __asyncValues(feed), feed_3_1; feed_3_1 = yield __await(feed_3.next()), !feed_3_1.done;) {
                    let post = feed_3_1.value;
                    yield yield __await(post);
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (feed_3_1 && !feed_3_1.done && (_a = feed_3.return)) yield __await(_a.call(feed_3));
                }
                finally { if (e_3) throw e_3.error; }
            }
        });
    }
    getStoriesFeed() {
        return __asyncGenerator(this, arguments, function* getStoriesFeed_1() {
            var e_4, _a;
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/stories`;
            const feed = this._getFeed(address);
            try {
                for (var feed_4 = __asyncValues(feed), feed_4_1; feed_4_1 = yield __await(feed_4.next()), !feed_4_1.done;) {
                    let post = feed_4_1.value;
                    yield yield __await(post);
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (feed_4_1 && !feed_4_1.done && (_a = feed_4.return)) yield __await(_a.call(feed_4));
                }
                finally { if (e_4) throw e_4.error; }
            }
        });
    }
    getTrendingStoriesFeed() {
        return __asyncGenerator(this, arguments, function* getTrendingStoriesFeed_1() {
            var e_5, _a;
            const address = `${this.baseURL.origin + (this.baseURL.pathname || '')}/posts/stories/trending`;
            const feed = this._getFeed(address);
            try {
                for (var feed_5 = __asyncValues(feed), feed_5_1; feed_5_1 = yield __await(feed_5.next()), !feed_5_1.done;) {
                    let post = feed_5_1.value;
                    yield yield __await(post);
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (feed_5_1 && !feed_5_1.done && (_a = feed_5.return)) yield __await(_a.call(feed_5));
                }
                finally { if (e_5) throw e_5.error; }
            }
        });
    }
}

class Hashnode {
    constructor(domain, path = '/') {
        if (path[0] !== '/') {
            path = '/' + path;
        }
        this.baseURL = new URL('', domain + path);
        this.feed = new Feed(this.baseURL);
        this.post = new Post(this.baseURL);
        this.user = new User(this.baseURL);
    }
    setProxy(proxyAddr) {
        this.feed.proxy = proxyAddr;
        this.post.proxy = proxyAddr;
        this.user.proxy = proxyAddr;
    }
}

exports.Hashnode = Hashnode;
