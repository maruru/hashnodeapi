const clear = require('rollup-plugin-clear');
const progress = require('rollup-plugin-progress');
const rollup = require('rollup').rollup;
const typescript = require('rollup-plugin-typescript2');


const isBrowser = process.argv.includes('-browser');

async function build() {
    const bundle = await rollup({
        external: ['superagent', 'superagent-proxy'],
        input: 'src/index.ts',
        plugins: [
            clear({
                targets: ['./dist'],
                watch: false,
            }),
            progress({
                clearLine: false,
            }),
            typescript({
                tsconfig: `tsconfig${ isBrowser ? '-browser' : '' }.json`
            }),
        ],
    });

    await bundle.write({
        file: `dist/api${ isBrowser ? '-browser' : '' }.js`,
        format: isBrowser ? 'iife' : "cjs",
        name: 'Hashnode',
    });
}

build().catch(console.error);
