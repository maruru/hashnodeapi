// Create new API object and set the domain and namespace path
const hn = new (require('..').Hashnode).default();

// You may add a proxy
hn.setProxy(new URL('http://localhost:3128'));

// Then query the API by using high-level convenience methods
(async () => {
    const feed = hn.feed.getRecentFeed();
    const mostRecentPost = await feed.next();
    console.log(mostRecentPost.value.title);
})().catch(console.error);
