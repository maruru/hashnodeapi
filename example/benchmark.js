if (!global.gc) {
    console.error('Start benchmark with --expose-gc!');
    process.exit(1);
}

const hn = new (require('..').Hashnode).default();
const rounds = 10;

hn.setProxy(new URL('http://localhost:3128'));

(async () => {
    let accRoundTime = 0n;
    let end;
    let i;
    let start;

    console.log('# of rounds: ', rounds);

    for (i = 0; i < rounds; i++) {
        start = process.hrtime.bigint();
        await hn.feed.getRecentFeed().next();
        end = process.hrtime.bigint();

        if (i === 0) {
            console.log('First round time: ', (end - start) / 1000000n, 'ms');
        }

        accRoundTime += end - start;
        global.gc();
    }

    console.log('Average round time: ', accRoundTime / BigInt(rounds) / 1000000n, 'ms');
})().catch(console.error);

