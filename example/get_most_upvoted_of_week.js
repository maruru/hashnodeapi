// Create new API object and set the domain and namespace path
const hn = new (require('..').Hashnode).default();

// You may add a proxy
hn.setProxy(new URL('http://localhost:3128'));

// Then query the API by using high-level convenience methods
(async () => {
    const profile = await hn.user.getProfiles({
        sinceDate: '7d',
        sortBy: "helpful",
        limit: 1,
    });

    console.log(profile[0].name);
})().catch(console.error);
