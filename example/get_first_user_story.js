// Create new API object and set the domain and namespace path
const hn = new (require('..').Hashnode).default();

// You may add a proxy
hn.setProxy(new URL('http://localhost:3128'));

// Then query the API by using high-level convenience methods
(async () => {
    const stories = (await hn.user.getStories('maruru'));
    let firstStory = undefined;

    console.log(`Found ${stories.length} stories!`);
    stories.forEach(story => {
        if (!firstStory) {
            firstStory = story;
            return;
        }

        if (firstStory.dateAdded > story.dateAdded) {
            firstStory = story;
        }
    });

    console.log(firstStory.title);
})().catch(console.error);
